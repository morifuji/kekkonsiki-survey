import axios from "axios";
const createCsvWriter = require("csv-writer").createArrayCsvWriter;

(async () => {

  const accessToken = "";

  // #結婚式
  //   let targetUrl = `https://graph.facebook.com/v9.0/17843882041046684/recent_media?user_id=17841413879894854&limit=50&fields=caption,timestamp&access_token=${accessToken}`;
  // #結婚式延期
  let targetUrl = `https://graph.facebook.com/v9.0/17841541513099488/recent_media?user_id=17841413879894854&limit=50&fields=caption,timestamp&access_token=${accessToken}`;

  let index = 1;
  while (true) {
    const response = await axios.get(targetUrl);

    const postList = response.data.data as Object[];

    const formeatted = postList.map((post: any) => {
      return [post.id, post.caption.replace(/\n/g, " "), post.timestamp];
    });

    const csvWriter = createCsvWriter({
      append: true,
      path: "log.csv",
      header: ["id", "content", "timestamp"],
    });

    console.log(formeatted);

    await csvWriter.writeRecords(formeatted);

    const nextUrl = response.data.paging?.next;
    if (nextUrl === undefined || nextUrl === null || nextUrl === "") {
      console.log("end");
      break;
    }

    console.log("got to next page", index);

    index++;
    targetUrl = nextUrl;
  }
})();
